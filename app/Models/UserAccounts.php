<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserAccounts extends Model
{
    protected $table="useraccounts";
    protected $fillable=['user_name','user_email','contact','address'];
}
