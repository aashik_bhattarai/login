<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"

          content="width=device-width, user-scalable=no,

initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome:: @yield('title',$title)</title>
    <link rel="stylesheet" href="{{url('lib/bootstrap/css/bootstrap.css')}}">
</head>
<body>
<div class="container">
    <div class="rom">
        <h1>Users Table</h1>
        <br>
        <a href={{route('welcome')}}>home</a>
        <a href={{route('contact')}}>home</a>
        <a href={{route('home')}}>home</a>
        <hr>
        @yield ('container')
    </div>

</div>
</body>
</html>
