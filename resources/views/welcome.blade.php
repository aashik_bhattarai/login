@extends ('layout.master')
@section('container')
    <div class="row">
        <div class="col-nd-4">
         <form action="" method="post" enctype="multipart/form-data">
             <div class="form-group">
                 <label for="name">Name</label>
                 <input type="text" name="name" id="name" class="form-control">

             </div>
             <div class="form-group">
                 <label for="image">Profile Picture</label>
                 <input type="file" name="image" id="email" class="btn btn-primary">
             </div>
             <div class="form-group">
                 <label for="password">Password</label>
                 <input type="password" name="password" id="password" class="form-control">
             </div>
             <div class="form-group">
                 <label for="cpassword">Password</label>
                 <input type="password" name="cpassword" id="cpassword" class="form-control">
             </div>
             <div class="form-group">
                 <button class="btn-primary">Register</button>
             </div>
         </form>
        </div>
    </div>
    <div class="col-nd-8">
        <table class="table" table=hover">
            <tr>
                <th>S.no</th>
                <th>Name</th>
                <th>Email</th>
                <th>Image</th>
                <th>Action</th>
                <th>Created At</th>
            </tr>
            <tr>
                <td>101</td>
                <td>Bhim Shah</td>
                <td>a@gmail.com</td>
                <td>Image Name</td>
                <td>
                    <button class="btn-danger">Delete</button>
                    <button class="btn-danger-primary">Edit</button>
                </td>
                <td>12:00 Sep 30</td>
            </tr>
            @foreach($userData as $key=>$userDatum)
                <tr>
                    <td>{{++$key}}</td>
                    <td>{{$userDatum->name}}</td>
                    <td>{{$userDatum->email}}</td>
                    <td>
                        <img src="{{url('lib/images/'.$userDatum->image)}}" height="20" width="30" alt="">
                    </td>
                    <td>{{$userDatum->created_at->DiffForHumans()}}</td>
                </tr>
            @endforeach
        </table>
        {{$userData->links()}} {{--for paginaiton--}}
    </div>
@stop
